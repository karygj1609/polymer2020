var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000

var path = require('path')

var cors = require('cors')
app.use(cors())


var cookieParser = require('cookie-parser')
app.use(cookieParser()) //interpretar cookies
//var bodyParser = require('body-parser')

//static files
app.use(express.static(__dirname + '/build/default'))


app.listen(port)

console.log('Proyecto Polymer con wrapper de NodeJS escuchando en: ' + port)

// app.get('/', function(req, res){
//   res.sendFile("index.html", {root:'.'})
// }
